(function($) {
  /**
   * Products collection
   * @type {{fetch}}
   */
  var productsCollection = (function() {
    function getProducts() {
      return $.ajax({
        dataType: 'json',
        url: './api/products.json'
      });
    }

    return {
      fetch: getProducts
    }
  })();

  /**
   * Cart controller
   * @type {{getProductCost, getSubtotal, getVat, getTotal, getCurrency}}
   */
  var cartController = (function() {
    var globalVat = 0.2;

    function getProductCost(price, qty) {
      return Number(price * qty).toFixed(2);
    }

    function getSubtotal(products) {
      var subtotal = 0;

      $.each(products, function(index, product) {
        subtotal += product.price * product.qty;
      });

      return subtotal;
    }

    function getVat(subtotal) {
      return Number(subtotal * globalVat).toFixed(2);
    }

    function getTotal(subtotal, vat) {
      var total = Number(subtotal) + Number(vat);
      return Number(total).toFixed(2);
    }

    return {
      getProductCost: getProductCost,
      getSubtotal: getSubtotal,
      getVat: getVat,
      getTotal: getTotal
    }
  })();

  /**
   * Cart view
   * @type {{init}}
   */
  var view = (function() {
    var currencySymbol = '&pound;',
      ui = {
        main: 'main',
        productsTable: '#products-table',
        productsTableContents: '#products-table-contents',
        product: '.product',
        cost: '[data-cost]',
        price: '[data-price]',
        qtyInput: '.qty-input',
        remove: '.remove',
        subtotal: '[data-subtotal]',
        vat: '[data-vat]',
        total: '[data-total]',
        checkout: '#checkout'
      },
      products = null;

    function composeProductsTableRow(product) {
      var qtyColumn = '<td data-th="Qty"><input class="qty-input" type="number" value="1" min="1"></td>',
        costColumn = '<td data-th="Cost" data-cost="' + product.price + '" class="cost">' + currencySymbol + product.price + '</td>',
        removeColumn = '<td><button class="button remove">Remove</button></td>',
        row = '';

      row += '<tr class="product" data-id="' + product.id + '">';
      row += '<td data-th="Product">' + product.name + ', ' + product.size + '</td>';
      row +=
        '<td data-th="Price" data-price="' + product.price + '" class="price">' + currencySymbol + product.price + '</td>';
      row += qtyColumn;
      row += costColumn;
      row += removeColumn;
      row += '</tr>';

      return row;
    }

    function updateTotals(subtotal) {
      var $subtotal = $(ui.subtotal),
        $vat = $(ui.vat),
        $total = $(ui.total),
        fixedSubtotal = Number(subtotal).toFixed(2),
        vat = cartController.getVat(subtotal),
        total = cartController.getTotal(subtotal, vat);

      $subtotal.html(currencySymbol + fixedSubtotal).data('subtotal', fixedSubtotal);
      $vat.html(currencySymbol + vat).data('vat', vat);
      $total.html(currencySymbol + total).data('total', total);
    }

    function renderProductsTable(selector) {
      productsCollection.fetch().done(function(data) {
        $(selector).empty();
        products = $.map(data, function(product) {
          var row = composeProductsTableRow(product);
          product.qty = 1;
          $(selector).append(row);

          return product;
        });

        updateTotals(cartController.getSubtotal(products));
      });
    }

    function listenQtyChangeEvents() {
      $(document).on('change', ui.qtyInput, function(e) {
        var qty = $(this).val(),
          price = $(this).parents('tr').find(ui.price).data('price'),
          id = $(this).parents('tr').data('id'),
          cost = cartController.getProductCost(price, qty);

        $.grep(products, function(product) {
          if (product.id === id) {
            product.qty = qty;
          }
        });

        $(this).parents('tr').find(ui.cost).html(currencySymbol + cost).data('cost', cost);

        updateTotals(cartController.getSubtotal(products));
      });
    }

    function listenProductRemoveEvents() {
      $(document).on('click', ui.remove, function(e) {
        var $row = $(this).parents('tr'),
          id = $(this).parents('tr').data('id');

        products = $.grep(products, function(product) {
          return product.id !== id;
        });

        $row.remove();

        updateTotals(cartController.getSubtotal(products));

        if (!products.length) {
          $(ui.checkout).off('click').addClass('disabled');
          $(ui.productsTableContents).replaceWith('<tr><td colspan="4" class="notification"><strong>No products in cart!</strong></td><td></td></tr>');
        }
      });
    }

    function listenCheckoutEvent() {
      $(ui.checkout).one('click', function(e) {
        var subtotal = cartController.getSubtotal(products),
          vat = cartController.getVat(subtotal),
          total = cartController.getTotal(subtotal, vat);

        $.ajax({
          type: 'post',
          url: './api/checkout.js',
          data: {
            products: products,
            subtotal: subtotal,
            vat: vat,
            total: total
          }
        }).done(function(data) {
          $(ui.main).html('<p class="notification"><strong>Thank you for your purchase!</strong></p>');
        }).fail(function(jqXHR, textStatus) {
          $(ui.main).html('<p class="notification"><strong>Request failed: ' + + textStatus + '</strong></p>');
        });

      });
    }

    function init() {
      renderProductsTable(ui.productsTableContents);
      listenQtyChangeEvents();
      listenProductRemoveEvents();
      listenCheckoutEvent();
    }

    return {
      init: init
    }
  })();

  // Initialize view
  view.init();

})(jQuery);
