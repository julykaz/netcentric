# Netcentric - Your Basket Web Developer Exercise v1.2
done by [Julius Morkūnas](http://morkunas.net)

## Building instructions
1. Install node, npm and gulp if don't have already.
2. run `npm build`.
3. Profit!

## Tools used
### Software
* WebStorm
* Photoshop

### Libraries
* jQuery

### Development dependencies
* gulp
* gulp-sass
* gulp-sourcemaps
